open TypeClasses

module Identity : sig
  type 'a t
  include Functor.S with type 'a t := 'a t
  include Applicative.S with type 'a t := 'a t
  include Monad.S with type 'a t := 'a t
end = struct
  module Base = struct
    type 'a t = 'a
    let fmap f a = f a
  end

  module IdentityFunctor = struct
    type 'a t = 'a
    include Functor.Make (Base)
    let pure a = a
    let apply f a = f a
  end

  module IdentityApplicative = struct
    type 'a t = 'a
    include Applicative.Make (IdentityFunctor)
    let bind a f = f a
  end

  include Base
  include Monad.Make (IdentityApplicative)
end

module Option : sig
  include module type of Option
  type 'a t = 'a option
  include Functor.S with type 'a t := 'a t
  include Applicative.S with type 'a t := 'a t
  include Monad.S with type 'a t := 'a t
end = struct
  module Base = struct
    type 'a t = 'a option
    let fmap = Option.map
  end

  module OptionFunctor = struct
    type 'a t = 'a option
    include Functor.Make (Base)
    let pure a = Some a
    let apply af a =
      match af, a with
      | Some f, Some x -> Some (f x)
      | _ -> None
  end

  module OptionApplicative = struct
    type 'a t = 'a option
    include Applicative.Make (OptionFunctor)
    let bind = Option.bind
  end

  include Monad.Make (OptionApplicative)
  include Option
end

module Result : sig
  include module type of Result
  type ('e, 'a) t = ('a, 'e) result
  include Functor.S2 with type ('e, 'a) t := ('e, 'a) t
  include Applicative.S2 with type ('e, 'a) t := ('e, 'a) t
  include Monad.S2 with type ('e, 'a) t := ('e, 'a) t
end = struct
  module Base = struct
    type ('e, 'a) t = ('a, 'e) result
    let fmap = Result.map
  end

  module ResultFunctor = struct
    type ('e, 'a) t = ('a, 'e) result
    include Functor.Make2 (Base)

    let pure a = Ok a
    let apply af a =
      match af, a with
      | Ok f, Ok x -> Ok (f x)
      | Error e, _ | Ok _, Error e -> Error e
  end

  module ResultApplicative = struct
    include Applicative.Make2 (ResultFunctor)
    let bind = Result.bind
  end

  include Result
  include Base
  include Monad.Make2 (ResultApplicative)
end

module List : sig
  include module type of List
  type 'a t = 'a list
  include Functor.S with type 'a t := 'a t
  include Applicative.S with type 'a t := 'a t
  include Monad.S with type 'a t := 'a t
end = struct
  module Base = struct
    type 'a t = 'a list
    let fmap = List.map
  end

  module ListFunctor = struct
    type 'a t = 'a list
    include Functor.Make (Base)
    let pure a = [a]
    let apply af a =
      List.concat @@ fmap (fun f ->
        fmap (fun x ->
          f x) a) af
  end

  module ListApplicative = struct
    type 'a t = 'a list
    include Applicative.Make (ListFunctor)
    let bind a f = List.concat @@ fmap f a
  end

  include Monad.Make (ListApplicative)
  include List
end

module StateT = struct
  module type S = sig
    type ('s, 'a) t
    type 'a m
    include Functor.S2 with type ('s, 'a) t := ('s, 'a) t
    include Applicative.S2 with type ('s, 'a) t := ('s, 'a) t
    include Monad.S2 with type ('s, 'a) t := ('s, 'a) t
    val run : ('s, 'a) t -> 's -> ('a * 's) m
    val eval : ('s, 'a) t -> 's -> 'a m
    val exec : ('s, 'a) t -> 's -> 's m
    val get : ('s, 's) t
    val put : 's -> ('s, unit) t
  end

  module Make (M : Monad.S) : S = struct
    module Base = struct
      type ('s, 'a) t = 's -> ('a * 's) M.t
      let fmap f s sa = M.(let* b, sb = s sa in return (f b, sb))
    end

    module StateTFunctor = struct
      type ('s, 'a) t = 's -> ('a * 's) M.t
      include Functor.Make2 (Base)

      let pure a s = M.return (a, s)
      let apply af a s =
        let open M in
        let* f, sa = af s in
        let* b, sb = a sa in
        return (f b, sb)
    end

    module StateTApplicative = struct
      include Applicative.Make2 (StateTFunctor)
      let bind m k s = M.(let* a, sa = m s in (k a) sa)
    end

    type 'a m = 'a M.t
    type ('s, 'a) t = 's -> ('a * 's) m
    include Monad.Make2 (StateTApplicative)

    let run s = s
    let eval st s = M.(fst <$> run st s)
    let exec st s = M.(snd <$> run st s)
    let get s = M.return (s, s)
    let put s = fun _ -> M.return ((), s)
  end
end

module ExceptT = struct
  module type S = sig
    type ('e, 'a) t
    type 'a m
    include Functor.S2 with type ('e, 'a) t := ('e, 'a) t
    include Applicative.S2 with type ('e, 'a) t := ('e, 'a) t
    include Monad.S2 with type ('e, 'a) t := ('e, 'a) t
    val run : ('e, 'a) t -> ('e, 'a) Result.t m
  end

  module Make (M : Monad.S) : S = struct
    module Base = struct
      type ('e, 'a) t = ('e, 'a) Result.t M.t
      let fmap f ex = M.fmap (Result.fmap f) ex
    end

    module ExceptTFunctor = struct
      type ('e, 'a) t = ('e, 'a) Result.t M.t
      include Functor.Make2 (Base)

      let pure a = M.return @@ Result.return a
      let apply af a =
        let open M in
        let* f = af in
        let* b = a in
        return Result.(f <*> b)
    end

    module ExceptTApplicative = struct
      include Applicative.Make2 (ExceptTFunctor)
      let bind m f =
        let open M in
        let* a = m in
        match a with
        | Ok x -> f x
        | Error e -> return @@ Error e
    end

    type 'a m = 'a M.t
    type ('e, 'a) t = ('e, 'a) Result.t M.t
    include Monad.Make2 (ExceptTApplicative)

    let run a = a
  end
end

module State = StateT.Make (Identity)
